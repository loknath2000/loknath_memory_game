const cards = document.querySelectorAll('.memory-box');
const button = document.querySelector("#button")

let FlippedCard = false;
let boardLock = false;
let firstCard, secondCard;


// function to adding class to element after clicked if it was clicked we will perform flip
function flipCard() {
    if (boardLock===true){    /* clicking the third box stopping those */
        return
    }
    if (this === firstCard){  /* condition for not to perfom double click */
        return
    }
    this.classList.add('flip');

    if (FlippedCard===false) {
        FlippedCard = true;
        firstCard = this;
    } else {
        hasFlippedCard=false;
        secondCard = this;
    }
    checkForMatch();
    
}

// function to check both images are exact match

function checkForMatch() {
    let isMatch = firstCard.dataset.type === secondCard.dataset.type;

    isMatch ? disableCards() : unflipCards();
}

// function to remove click event on the cards after matched
function disableCards() {
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);

    resetBoard();
}

// function to unflip cards if it was not matched

function unflipCards() {
    boardLock=true;
    setTimeout(() => {
        firstCard.classList.remove('flip');
        secondCard.classList.remove('flip');
        resetBoard();
    }, 1000);
}

// function to reset board after each matching happens

function resetBoard() {
    [FlippedCard,boardLock] = [false,false];
    [firstCard, secondCard] = ["", ""];
}


// funciton to shuffle the images 


function shuffle() {
    cards.forEach(card => {
        let randomPosition = Math.floor(Math.random() * 16);
        card.style.order = randomPosition;
    });
};


// iterating through each card doing function on click using flipcard function as argument to event listener

cards.forEach(card => card.addEventListener('click', flipCard));

// button to restart the game

button.addEventListener("click", () => {
    location.reload();
})

// calling shuffle function to perform shuffling operation
shuffle();